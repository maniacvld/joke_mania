package ro.maniac.jokemania;

import android.app.Application;
import android.util.Log;
import ro.maniac.jokemania.api.AddCommentTask;
import ro.maniac.jokemania.api.AddJokeTask;
import ro.maniac.jokemania.api.GetFeedTask;
import ro.maniac.jokemania.models.CommentModel;
import ro.maniac.jokemania.models.JokeModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * asta se creaza in momentul in care se creaza aplicatia pentru prima data
 * si exista o singura instanta a clasei intotdeauna
 * (deci putem face un fel de hibrid de singleton)
 */
public class JokeMania extends Application {

    private static JokeMania instance;

    public static JokeMania getInstance() {
        return instance;
    }

    private List<JokeModel> jokes; // astea-s bankurile incarcate
    private JokeModel selectedJoke; // asta-i bankul selectat...
    
    private JokesChangedListener jokesChangedListener;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public void fetchJokes() {
        Log.d("JM", "FETCH JOKES");
        //* remove first '/' to toggle between live and test data
        new GetFeedTask(this).start();
        /*/
        List<JokeModel> jokes = new ArrayList<JokeModel>();
        JokeModel model = new JokeModel();
        model.setDateAdded(1325628530000l);
        model.setJoke("Un scotian impreuna cu sotia si copilasul merg la un concert de muzica clasica. Plasatoarea ii spune:<br/>\n" +
                "- Daca cumva copilasul zbiara, trebuie sa parasiti sala. Bineinteles banii pentru bilete ii veti primi inapoi…\n" +
                "Catre sfirsitul concertului, scotianul i se adreseaza sotiei:\n" +
                "- Mai e mult?\n" +
                "- Citeva minute doar…\n" +
                "- Bine, atunci smuceste-l un pic pe junior.");
        List<CommentModel> comments = new ArrayList<CommentModel>();
        comments.add(new CommentModel("Ceva user", "un comment scurt", 1325628330000l));
        comments.add(new CommentModel("Anonim", "ceva mai lungut un pic, sa vedem cum functioneaza cand trebuie sa fie pe mai multe randuri", 1325628630000l));
        model.setComments(comments);

        jokes.add(new JokeModel(1325628508l, "This is a really short joke"));
        jokes.add(model);
        jokes.add(new JokeModel(1325628543l, "And if that isn't long enough than this will be: fkjlda jskfld jsaiof djsioa fjdkls; ajkld jifo asdjiof jakdsl a;jkfdl jsaof disoa jfdhsajlk fhdjisa fhdihwuief"));
        jokes.add(new JokeModel(1325628551l, "Another small joke"));
        setJokes(jokes);
        //*/
    }
    
    public List<JokeModel> getJokes() {
        return jokes;
    }

    public void setJokes(List<JokeModel> jokes) {
        this.jokes = jokes;
        if (jokesChangedListener != null) {
            jokesChangedListener.jokesChanged();
        }
    }

    public JokeModel getSelectedJoke() {
        return selectedJoke;
    }

    public void setSelectedJoke(JokeModel selectedJoke) {
        this.selectedJoke = selectedJoke;
    }

    public void saveJoke(String joke) {
        new AddJokeTask(this, joke).start();
    }
    
    public void setJokesChangedListener(JokesChangedListener listener) {
        this.jokesChangedListener = listener;
    }

    public void saveComment(String comment, String user, long id) {
        new AddCommentTask(this, user, comment, id).start();
    }

    public void reloadSelectedJoke() {
        if (selectedJoke == null || jokes == null) return;
        for (JokeModel joke : jokes) {
            if (joke.getId() == selectedJoke.getId()) {
                selectedJoke = joke;
            }
        }
    }
}
