package ro.maniac.jokemania.activities;

import android.app.Activity;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import ro.maniac.jokemania.JokeMania;
import ro.maniac.jokemania.R;

/**
 */
public class AddCommentPage extends BaseActivity implements View.OnClickListener {

    private Button saveButton;
    private EditText commentText;
    private EditText userText;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_comment_page);

        saveButton = (Button) findViewById(R.id.save_button);
        saveButton.setOnClickListener(this);

        commentText = (EditText) findViewById(R.id.add_comment_text);
        userText = (EditText) findViewById(R.id.add_comment_user_text);
    }

    @Override
    public boolean handleMessage(Message message) {
        if (message.what == WHAT_JOKES_CHANGED) {
            // nee to reload the comments
            JokeMania.getInstance().reloadSelectedJoke();
            finish();
        }
        return super.handleMessage(message);
    }

    public void onClick(View view) {
        if (view.equals(saveButton)) {
            // save the joke...
            JokeMania.getInstance().saveComment(commentText.getText().toString(), userText.getText().toString(),
                    JokeMania.getInstance().getSelectedJoke().getId());
            // set a modal dialog that dissappears when the joke has been saved
        }
    }
}