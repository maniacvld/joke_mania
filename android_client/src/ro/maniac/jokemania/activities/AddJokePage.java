package ro.maniac.jokemania.activities;

import android.app.Activity;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import ro.maniac.jokemania.JokeMania;
import ro.maniac.jokemania.R;

/**
 */
public class AddJokePage extends BaseActivity implements View.OnClickListener {
    
    private Button saveButton;
    private EditText jokeText;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_joke_page);
    
        saveButton = (Button) findViewById(R.id.save_button);
        saveButton.setOnClickListener(this);

        jokeText = (EditText) findViewById(R.id.add_joke_text);
    }

    @Override
    public boolean handleMessage(Message message) {
        if (message.what == WHAT_JOKES_CHANGED) {
            finish();
        }
        return super.handleMessage(message);
    }

    public void onClick(View view) {
        if (view.equals(saveButton)) {
            // save the joke...
            JokeMania.getInstance().saveJoke(jokeText.getText().toString());
            // set a modal dialog that dissappears when the joke has been saved
        }
    }
}