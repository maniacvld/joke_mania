package ro.maniac.jokemania.activities;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import ro.maniac.jokemania.JokeMania;
import ro.maniac.jokemania.JokesChangedListener;

/**
 * activitate de baza cu un handler atasat de threadul de UI pentru a primi evenimente
 */
public abstract class BaseActivity extends Activity implements Handler.Callback, JokesChangedListener {

    // constante pentru mesaje trimise prin handler
    public static final int WHAT_START_JOKES = 1;
    public static final int WHAT_JOKES_CHANGED = 2;
    
    protected Handler handler;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = new Handler(this);
        JokeMania.getInstance().setJokesChangedListener(this);
    }

    public boolean handleMessage(Message message) {
        return false;
    }

    public void jokesChanged() {
        handler.sendEmptyMessage(WHAT_JOKES_CHANGED);
    }
}
