package ro.maniac.jokemania.activities;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import ro.maniac.jokemania.JokeMania;
import ro.maniac.jokemania.R;

import java.io.IOException;
import java.io.InputStreamReader;

/**
 */
public class InfoPage extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info_page);
        WebView wv = (WebView) findViewById(R.id.webview);
        loadWebContentInWebView(wv, "info.html");
    }

    /**
     * Load the html file located in the assets folder, in the appropriate webView
     *
     * @param webView
     * @param htmlFile
     */
    public static void loadWebContentInWebView(WebView webView, String htmlFile) {

        String htmlContent = "";
        try {
            char[] buffer = new char[1024];
            int count;
            InputStreamReader ir = new InputStreamReader(JokeMania.getInstance().getAssets().open(htmlFile));
            StringBuilder sb = new StringBuilder();
            while ((count = ir.read(buffer)) != -1) {
                sb.append(buffer, 0, count);
            }
            if (sb.length() != 0) {
                htmlContent = sb.toString();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        webView.loadDataWithBaseURL("file:///android_res/drawable/", htmlContent.replace("\r", ""), "text/html",
                "UTF-8", null);
        webView.setHorizontalScrollbarOverlay(false);
        webView.setVerticalScrollbarOverlay(true);
    }
}