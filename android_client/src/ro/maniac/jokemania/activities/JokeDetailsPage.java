package ro.maniac.jokemania.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import ro.maniac.jokemania.JokeMania;
import ro.maniac.jokemania.R;
import ro.maniac.jokemania.adapters.CommentAdapter;
import ro.maniac.jokemania.adapters.JokesAdapter;
import ro.maniac.jokemania.adapters.MergeAdapter;
import ro.maniac.jokemania.models.CommentModel;
import ro.maniac.jokemania.models.JokeModel;

import java.util.ArrayList;
import java.util.List;

/**
 * pagina cu detaliile bankului si comment-urile
 */
public class JokeDetailsPage extends Activity implements View.OnClickListener {

    private MergeAdapter adapter;
    private JokesAdapter singleJokeAdapter;
    private CommentAdapter commentsAdapter;
    private Button backButton;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.joke_details_page);

        singleJokeAdapter = new JokesAdapter(this);
        commentsAdapter = new CommentAdapter(this);
        adapter = new MergeAdapter();
        adapter.addAdapter(singleJokeAdapter);
        adapter.addAdapter(commentsAdapter);

        ListView detailsList = (ListView) findViewById(R.id.jokes_detail_list);
        detailsList.setAdapter(adapter);

        backButton = (Button) findViewById(R.id.back_button);
        backButton.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setData(JokeMania.getInstance().getSelectedJoke());
    }

    private void setData(JokeModel model) {
        // set the joke and the comments and add them to the adapter.
        List<JokeModel> oneJokeList = new ArrayList<JokeModel>();
        oneJokeList.add(model);
        singleJokeAdapter.setItems(oneJokeList);
        commentsAdapter.setItems(model.getComments());
    }


    public void onClick(View view) {
        if (view.equals(backButton)) {
            finish(); // butonu' asta-i doar de convenienta...
        }
    }

    /**
     * creates the menu displayed when the user clicks on the hardware menu button.
     * @param menu the isntance of the menu object that holds the options menu.
     * @return true if the menu needs to be shown, false otherwise
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.joke_details_menu, menu);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_add:
                startActivity(new Intent(this, AddCommentPage.class));
                return true;
            case R.id.menu_info:
                startActivity(new Intent(this, InfoPage.class));
                return true;
        }
        return true;
    }
}