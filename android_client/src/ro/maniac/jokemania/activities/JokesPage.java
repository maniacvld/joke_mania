package ro.maniac.jokemania.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import ro.maniac.jokemania.JokeMania;
import ro.maniac.jokemania.R;
import ro.maniac.jokemania.adapters.JokesAdapter;
import ro.maniac.jokemania.models.JokeModel;

import java.util.ArrayList;
import java.util.List;

/**
 * activitate pentru pagina de bankuri
 */
public class JokesPage extends BaseActivity implements AdapterView.OnItemClickListener {

    private JokesAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.jokes_page);

        adapter = new JokesAdapter(this);

        ListView jokesList = (ListView) findViewById(R.id.jokes_list);
        jokesList.setAdapter(adapter);
        jokesList.setOnItemClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.setItems(JokeMania.getInstance().getJokes());
    }

    @Override
    public boolean handleMessage(Message message) {
        if (message.what == WHAT_JOKES_CHANGED) {
            adapter.setItems(JokeMania.getInstance().getJokes());
        }
        return super.handleMessage(message);
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        JokeMania.getInstance().setSelectedJoke(adapter.getItem(i));
        // incepem activitatea pentru a vedea detaliile
        Intent intent = new Intent(this, JokeDetailsPage.class);
        startActivity(intent);
    }

    /**
     * creates the menu displayed when the user clicks on the hardware menu button.
     * @param menu the isntance of the menu object that holds the options menu.
     * @return true if the menu needs to be shown, false otherwise
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.jokes_menu, menu);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_add:
                startActivity(new Intent(this, AddJokePage.class));
                return true;
            case R.id.menu_info:
                startActivity(new Intent(this, InfoPage.class));
                return true;
        }
        return true;
    }
}
