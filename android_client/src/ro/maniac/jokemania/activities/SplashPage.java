package ro.maniac.jokemania.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import ro.maniac.jokemania.JokeMania;
import ro.maniac.jokemania.R;

public class SplashPage extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        JokeMania.getInstance().fetchJokes();
        // wait a few seconds and show the main page
        this.handler.sendEmptyMessageDelayed(WHAT_START_JOKES, 3000);
    }

    @Override
    public boolean handleMessage(Message message) {
        if (message.what == WHAT_START_JOKES) {
            Intent intent = new Intent(this, JokesPage.class);
            startActivity(intent);
            finish(); // inchidem activitatea asta, ca nu mai avem nevoie de ea...
        }
        return super.handleMessage(message);
    }
}
