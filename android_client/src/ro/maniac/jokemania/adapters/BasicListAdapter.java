package ro.maniac.jokemania.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import ro.maniac.jokemania.models.Model;
import ro.maniac.jokemania.views.BasicListItem;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: codetwister
 * Date: 1/4/12
 * Time: 12:29 AM
 * To change this template use File | Settings | File Templates.
 */
public abstract class BasicListAdapter<T extends Model, V extends BasicListItem<T>> extends BaseAdapter {

    private Context context;
    private List<T> items;

    protected BasicListAdapter(Context context) {
        this.context = context;
    }

    public int getCount() {
        if (items == null) return 0;
        return items.size();
    }

    public T getItem(int i) {
        return items.get(i);
    }

    public long getItemId(int i) {
        // nu o sa folosim asta.
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        T object = getItem(i);
        V resultView;
        if (view != null) {
            resultView = (V) view;
        } else {
            resultView = (V) View.inflate(context, getListItemResourceId(), null);
        }
        resultView.setValues(object);
        return resultView;
    }

    public void setItems(List<T> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    protected abstract int getListItemResourceId();
}
