package ro.maniac.jokemania.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import ro.maniac.jokemania.R;
import ro.maniac.jokemania.models.CommentModel;
import ro.maniac.jokemania.views.CommentView;

import java.util.List;

/**
 */
public class CommentAdapter extends BasicListAdapter<CommentModel, CommentView> {

    public CommentAdapter(Context context) {
        super(context);
    }

    @Override
    protected int getListItemResourceId() {
        return R.layout.comment_li;
    }

    @Override
    public int getItemViewType(int position) {
        // asta-i necesar ca adapter-ul sa stie ce tip de view refoloseste pentru
        // ce element mai ales cand sunt mai multe tipuri de view-uri in lista
        return 1;
    }
}
