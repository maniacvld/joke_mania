package ro.maniac.jokemania.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import ro.maniac.jokemania.R;
import ro.maniac.jokemania.models.JokeModel;
import ro.maniac.jokemania.views.JokeView;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: codetwister
 * Date: 1/3/12
 * Time: 11:43 PM
 */
public class JokesAdapter extends BasicListAdapter<JokeModel, JokeView> {

    public JokesAdapter(Context context) {
        super(context);
    }

    @Override
    protected int getListItemResourceId() {
        return R.layout.joke_li;
    }

    @Override
    public int getItemViewType(int position) {
        // asta-i necesar ca adapter-ul sa stie ce tip de view refoloseste pentru
        // ce element mai ales cand sunt mai multe tipuri de view-uri in lista
        return 0;
    }
}
