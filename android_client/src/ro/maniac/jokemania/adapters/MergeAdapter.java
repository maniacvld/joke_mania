package ro.maniac.jokemania.adapters;

import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Date: 9/5/11
 * Time: 4:44 PM
 * custom adapter that merges several other adapters.<br />
 * For viewtype style item recycling to work you have to add the adapter before you set the adapter to the list
 * and also make sure that the view types returned between adapters don't clash and it's zero based
 * i.e. viewType < viewTypeCount (if the view type returned is bigger than viewtypecount an exception is thrown)
 */
public class MergeAdapter extends BaseAdapter {

    private List<ListAdapter> listAdapters;
    private DataSetObserver dataSetObserver;

    public MergeAdapter() {
        dataSetObserver = new DataSetObserver() {
            @Override
            public void onChanged() {
                MergeAdapter.this.notifyDataSetChanged();
            }

            @Override
            public void onInvalidated() {
                MergeAdapter.this.notifyDataSetInvalidated();
            }
        };
    }

    public int getCount() {
        if (listAdapters == null) return 0;
        int totalCount = 0;
        for (ListAdapter adapter : listAdapters) {
            totalCount += adapter.getCount();
        }
        return totalCount;
    }

    private ListAdapter getAdapterAtIndex(int i) {
        int currentIndex = i;
        Iterator<ListAdapter> iterator = listAdapters.iterator();
        ListAdapter currentAdapter = iterator.next();
        while (currentIndex >= currentAdapter.getCount()) {
            if (iterator.hasNext()) {
                currentIndex -= currentAdapter.getCount();
                currentAdapter = iterator.next();
            } else {
                break;
            }
        }
        return currentAdapter;
    }

    private int getCurrentAdapterIndex(int referenceIndex) {
        int currentIndex = referenceIndex;
        Iterator<ListAdapter> iterator = listAdapters.iterator();
        ListAdapter currentAdapter = iterator.next();
        while (currentIndex >= currentAdapter.getCount()) {
            if (iterator.hasNext()) {
                currentIndex -= currentAdapter.getCount();
                currentAdapter = iterator.next();
            } else {
                return -1;
            }
        }
        return currentIndex;
    }

    public Object getItem(int i) {
        int currentIndex = getCurrentAdapterIndex(i);
        if (currentIndex < 0) {
            return null;
        }
        return getAdapterAtIndex(i).getItem(currentIndex);
    }

    public long getItemId(int i) {
        int currentIndex = getCurrentAdapterIndex(i);
        if (currentIndex < 0) {
            return -1;
        }
        return getAdapterAtIndex(i).getItemId(currentIndex);
    }

    @Override
    public int getItemViewType(int position) {
        int currentIndex = getCurrentAdapterIndex(position);
        if (currentIndex < 0) {
            return 0;
        }
        return getAdapterAtIndex(position).getItemViewType(currentIndex);
    }

    @Override
    public int getViewTypeCount() {
        int count = 0;
        for (ListAdapter listAdapter : listAdapters) {
            count += listAdapter.getViewTypeCount();
        }
        return count;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        return getAdapterAtIndex(i).getView(getCurrentAdapterIndex(i), view, viewGroup);
    }

    public void addAdapter(ListAdapter adapter) {
        if (listAdapters == null) {
            listAdapters = new ArrayList<ListAdapter>();
        }
        adapter.registerDataSetObserver(dataSetObserver);
        listAdapters.add(adapter);
    }

    public void addView(View v) {
        addAdapter(new SingleViewAdapter(v));
    }

    public static SingleViewAdapter createAdapter(View v) {
        return new SingleViewAdapter(v);
    }

    public static SingleViewAdapter createAdapter(int viewType, View v) {
        return new SingleViewAdapter(viewType, v);
    }

    public static class SingleViewAdapter extends BaseAdapter {

        private int viewType = 0;
        private boolean enabled = true;
        private View v;

        private SingleViewAdapter(View v) {
            this.v = v;
        }

        public SingleViewAdapter(int viewType, View v) {
            this.viewType = viewType;
            this.v = v;
        }

        public int getCount() {
            return enabled?1:0;
        }

        public Object getItem(int i) {
            return v;
        }

        public long getItemId(int i) {
            return 0;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            return v;
        }

        public void setViewType(int viewType) {
            this.viewType = viewType;
        }

        @Override
        public int getItemViewType(int position) {
            return viewType;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        public boolean isEnabled() {
            return enabled;
        }

        public void setEnabled(boolean enabled) {
            if (this.enabled != enabled) {
                this.enabled = enabled;
                notifyDataSetChanged();
            }
        }
    }
}
