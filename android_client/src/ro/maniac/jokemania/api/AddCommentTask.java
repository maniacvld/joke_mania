package ro.maniac.jokemania.api;

import android.content.Context;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ByteArrayEntity;
import org.json.JSONException;
import org.json.JSONObject;
import ro.maniac.jokemania.R;

/**
 */
public class AddCommentTask extends BaseApiTask {

    private String user;
    private String comment;
    private long jokeId;

    public AddCommentTask(Context context, String user, String comment, long jokeId) {
        super(context);
        this.user = user;
        this.comment = comment;
        this.jokeId = jokeId;
    }

    @Override
    protected HttpRequestBase getRequest(Context context) {
        return new HttpPost(context.getString(R.string.server_addr)+ADD_COMMENT_EXTENSION);
    }

    @Override
    protected HttpEntity getEntity() {
        JSONObject payload = new JSONObject();
        try {
            payload.put("user", user);
            payload.put("comment", comment);
            payload.put("joke_id", jokeId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return new ByteArrayEntity(payload.toString().getBytes());
    }
}
