package ro.maniac.jokemania.api;

import android.content.Context;
import android.util.Log;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import ro.maniac.jokemania.JokeMania;
import ro.maniac.jokemania.R;
import ro.maniac.jokemania.models.JokeModel;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 */
public class AddJokeTask extends BaseApiTask {

    private String joke;

    public AddJokeTask(Context context, String joke) {
        super(context);
        this.joke = joke;
    }

    @Override
    protected HttpRequestBase getRequest(Context context) {
        return new HttpPost(context.getString(R.string.server_addr)+ADD_JOKE_EXTENSION);
    }

    @Override
    protected HttpEntity getEntity() {
        JSONObject payload = new JSONObject();
        try {
            payload.put("joke", joke);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return new ByteArrayEntity(payload.toString().getBytes());
    }
}
