package ro.maniac.jokemania.api;

/**
 */
public interface ApiConstants {

    public static final int CONNECTION_TIMEOUT = 20 * 1000; // 20 seconds timeout should be more than enough
    public static final String FEED_EXTENSION = "FEED";
    public static final String ADD_JOKE_EXTENSION = "ADD_JOKE";
    public static final String ADD_COMMENT_EXTENSION = "ADD_COMMENT";

}
