package ro.maniac.jokemania.api;

import android.content.Context;
import android.util.Log;
import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import ro.maniac.jokemania.JokeMania;
import ro.maniac.jokemania.R;
import ro.maniac.jokemania.models.JokeModel;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 */
public abstract class BaseApiTask extends Thread implements ApiConstants {

    private Context context;

    public BaseApiTask(Context context) {
        this.context = context;
    }

    protected abstract HttpRequestBase getRequest(Context context);
    protected abstract HttpEntity getEntity();

    @Override
    public void run() {
        Log.d("JM", "STARTED THREAD");
        HttpClient client = new DefaultHttpClient();
        HttpParams params = client.getParams();
        HttpConnectionParams.setConnectionTimeout(params, CONNECTION_TIMEOUT);

        HttpRequestBase request = getRequest(context);
        HttpEntity entity = getEntity();
        if (entity != null && request instanceof HttpPost) {
            ((HttpPost) request).setEntity(entity);
        }
        try {
            HttpResponse response = client.execute(request);
            if (response != null) {
                JSONObject jsonObject = getJsonFromStream(response.getEntity().getContent());
                if (jsonObject != null && jsonObject.optInt("status", -1) >= 0) {
                    // yaaay it's working
                    JSONArray arr = jsonObject.optJSONArray("result");
                    List<JokeModel> jokes = new ArrayList<JokeModel>();
                    for (int i = 0; i < arr.length(); i++) {
                        JokeModel model = new JokeModel();
                        model.loadFromJson(arr.optJSONObject(i));
                        jokes.add(model);
                    }
                    JokeMania.getInstance().setJokes(jokes);
                } else {
                    // ceva eroare
                    Log.d("JM", "ERROR: "+((jsonObject != null)?jsonObject.optString("message"):"result is null"));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static JSONObject getJsonFromStream(InputStream in) throws IOException, JSONException {
        Log.d("JM", "started reading JSON");
        char[] buffer = new char[1024];
        int count;
        InputStreamReader ir = new InputStreamReader(in);
        StringBuilder sb = new StringBuilder();
        while ((count = ir.read(buffer)) != -1) {
            sb.append(buffer, 0, count);
        }
        if (sb.length() == 0) {
            Log.d("JM", "nothing to read");
            return null;
        }
        Log.d("JM", "response json: "+sb.toString());
        return new JSONObject(sb.toString());
    }

}
