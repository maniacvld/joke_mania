package ro.maniac.jokemania.api;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import ro.maniac.jokemania.JokeMania;
import ro.maniac.jokemania.R;
import ro.maniac.jokemania.models.JokeModel;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 */
public class GetFeedTask extends BaseApiTask {

    public GetFeedTask(Context context) {
        super(context);
    }

    @Override
    protected HttpRequestBase getRequest(Context context) {
        return new HttpGet(context.getString(R.string.server_addr)+FEED_EXTENSION);
    }

    @Override
    protected HttpEntity getEntity() {
        return null;
    }
}
