package ro.maniac.jokemania.models;

/**
 * model de date pentru comment-uri
 */
public class CommentModel implements Model {
    
    private long id;
    private String user;
    private String comment;
    private long dateAdded;

    public CommentModel() {
    }

    public CommentModel(String user, String comment, long dateAdded) {
        this.user = user;
        this.comment = comment;
        this.dateAdded = dateAdded;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public long getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(long dateAdded) {
        this.dateAdded = dateAdded;
    }
}
