package ro.maniac.jokemania.models;

import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * model de date pentru bank
 */
public class JokeModel implements Model {
    
    private long id;
    private String joke;
    private long dateAdded;
    private double rating; // de la 0 la 1
    private int ratingCount; // numarul de ratings
    private List<CommentModel> comments;

    public JokeModel() {
    }

    public JokeModel(long dateAdded, String joke) {
        this.dateAdded = dateAdded;
        this.joke = joke;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getJoke() {
        return joke;
    }

    public void setJoke(String joke) {
        this.joke = joke;
    }

    public long getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(long dateAdded) {
        this.dateAdded = dateAdded;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public int getRatingCount() {
        return ratingCount;
    }

    public void setRatingCount(int ratingCount) {
        this.ratingCount = ratingCount;
    }

    public List<CommentModel> getComments() {
        return comments;
    }

    public void setComments(List<CommentModel> comments) {
        this.comments = comments;
    }
    
    public void loadFromJson(JSONObject jsonObject) {
        id = jsonObject.optLong("id");
        joke = jsonObject.optString("joke");
        dateAdded = jsonObject.optLong("dateAdded");
        rating = jsonObject.optDouble("rating");
        ratingCount = jsonObject.optInt("ratingCount");
        comments = new ArrayList<CommentModel>();
        JSONArray commentsArr = jsonObject.optJSONArray("comments");
        Gson gson = new Gson();
        if (commentsArr != null) {
            for (int i = 0; i < commentsArr.length(); i++) {
                CommentModel comm = gson.fromJson(commentsArr.optJSONObject(i).toString(), CommentModel.class);
                comments.add(comm);
            }
        }
    }
}
