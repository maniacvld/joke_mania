package ro.maniac.jokemania.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import ro.maniac.jokemania.models.Model;

import java.text.SimpleDateFormat;

/**
 */
public abstract class BasicListItem<T extends Model> extends LinearLayout {

    protected static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public BasicListItem(Context context) {
        super(context);
    }

    public BasicListItem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    
    public abstract void setValues(T model);
}
