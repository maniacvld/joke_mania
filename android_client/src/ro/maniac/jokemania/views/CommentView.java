package ro.maniac.jokemania.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;
import ro.maniac.jokemania.R;
import ro.maniac.jokemania.models.CommentModel;

import java.util.Date;

/**
 */
public class CommentView extends BasicListItem<CommentModel> {

    private TextView userText;
    private TextView commentText;
    private TextView timeText;
    
    public CommentView(Context context) {
        super(context);
    }

    public CommentView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        userText = (TextView) findViewById(R.id.comment_user);
        commentText = (TextView) findViewById(R.id.comment_text);
        timeText = (TextView) findViewById(R.id.comment_time);
    }

    @Override
    public void setValues(CommentModel model) {
        userText.setText(model.getUser());
        commentText.setText(model.getComment());
        timeText.setText(sdf.format(new Date(model.getDateAdded()*1000)));
    }
}
