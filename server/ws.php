<?php

define("DB_NAME", "jokemania");
define("DB_HOST", "localhost");
define("DB_USER", "jokemania");
define("DB_PASS", "jokemania");

// setup response object
$responseObj = array();
$responseObj["status"] = 0;
$responseObj["message"] = "";

// connect to the database
$link = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

if (mysqli_connect_errno()) {
    $responseObj["status"] = -1; // -1 server error
    $responseObj["message"] = "Connect failed: ".mysqli_connect_error();
    echo json_encode($responseObj);
    exit();
}

if (isset($_GET["add"])) {
    $add = $_GET["add"];
    // decode the input
    $input = json_decode($HTTP_RAW_POST_DATA, true);
    if ($add == "joke") {
        // add a joke to the database
        $jokeRes = mysqli_query($link, "INSERT INTO `jokes` SET `joke` = '{$input['joke']}'");
        $responseObj["status"] = ($jokeRes!==false)?1:-1; // 1 joke add success
        $responseObj["message"] = ($jokeRes!==false)?"Joke added: ":"Could not add joke";
    } else if ($add == "comment") {
        // add a comment to the database
        $commentRes = mysqli_query($link, "INSERT INTO `comments` SET `user` = '{$input['user']}', `comment` = '{$input['comment']}', `joke_id` = '{$input['joke_id']}'");
        $responseObj["status"] = ($commentRes!==false)?2:-1; // 2 comment add success
        $responseObj["message"] = ($commentRes!==false)?"Comment added":"Could not add comment";
    }
}

// get latest 10 jokes
$jokesArr = array();
if ($jokeRes = mysqli_query($link, "SELECT `id`, UNIX_TIMESTAMP(`date_added`) as `dateAdded`, `joke`, `rating`, `rating_count` as `ratingCount` FROM `jokes` WHERE 1 ORDER BY `id` DESC LIMIT 0, 10")) {
    while (($row = mysqli_fetch_assoc($jokeRes)) != null) {
        $joke = $row;
        // TODO add comments
        if ($commentRes = mysqli_query($link, "SELECT `id`, UNIX_TIMESTAMP(`date_added`) as `dateAdded`, `user`, `comment` FROM `comments` WHERE `joke_id` = '".$joke["id"]."' ORDER BY `id` DESC")) {
            $commentsArr = array();
            while (($row = mysqli_fetch_assoc($commentRes)) != null) {
                $commentsArr[] = $row;
            }
            $joke["comments"] = count($commentsArr)>0?$commentsArr:null;
        }
        $joke["joke"] = utf8_encode($joke["joke"]);
        $jokesArr[] = $joke;
    }
    mysqli_free_result($jokeRes);
}

$responseObj["result"] = count($jokesArr)>0?$jokesArr:null;

echo json_encode($responseObj);

mysqli_close($link);

?>